var gulp 		= require('gulp'), //подключаем
	less 		= require('gulp-less'),
	browserSync = require('browser-sync'),
	concat 		= require('gulp-concat'),
	uglify 		= require('gulp-uglifyjs'),
	cssnano 	= require('gulp-cssnano'),
    rename 		= require('gulp-rename'),
    del 		= require('del'),
    cache       = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer');
    plumber = require('gulp-plumber');


gulp.task('less',function(){
	return gulp.src('app/less/**/*.less') //источник
		.pipe(plumber())
		.pipe(less()) // преобразуем
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
		/*.pipe(cssnano())*/
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('app/css/')) //выгружаем
		.pipe(browserSync.reload({stream:true})) //обновляем страницу

});

gulp.task('browser-sync', function(){
	browserSync({
		server: { //параметры сервера
			baseDir: 'app' //дирректория app
		},
		notify:false // уведомления
	});
});

gulp.task('scripts', function(){
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/libs/bootstrap/dist/js/bootstrap.min.js',
		'app/libs/air-datepicker/dist/js/datepicker.min.js',
		'app/libs/slick-carousel/slick/slick.min.js',
		'app/libs/wow/dist/wow.min.js'
		])
		.pipe(concat('libs.min.js'))
		/*.pipe(uglify())*/
		.pipe(gulp.dest('app/js'))
});

gulp.task('css-lib',function (){
	return gulp.src([
		'app/libs/bootstrap/dist/css/bootstrap.min.css',
		'app/libs/air-datepicker/dist/css/datepicker.min.css',
		'app/libs/slick-carousel/slick/slick.css',
		'app/libs/slick-carousel/slick/slick-theme.css',
		'app/libs/wow/css/libs/animate.css'
		])
		.pipe(concat('libs.min.css'))
		/*.pipe(cssnano())*/
		.pipe(gulp.dest('app/css'));
})


gulp.task('watch',['browser-sync', 'css-lib', 'less', 'scripts'], function() {
    gulp.watch('app/less/**/*.less', ['less']); //less файлы наблюдаем
    gulp.watch('app/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('app/js/**/*.js', browserSync.reload); // Наблюдение за JS файлами в папке js
});

gulp.task('clean', function() {
    return del.sync('dist');
});


gulp.task('build', ['clean', 'css-lib', 'less', 'scripts'], function(){

	gulp.src('app/css/*.css')
	.pipe(cssnano({ zindex: false }))
	.pipe(gulp.dest('dist/css'));

	gulp.src('app/js/*.js')
	.pipe(uglify())
    .pipe(gulp.dest('dist/js'));

	gulp.src('app/fonts/**/*')
	.pipe(gulp.dest('dist/fonts'));

    gulp.src('app/*.html') 
    .pipe(gulp.dest('dist'));

    gulp.src('app/*.*')
    .pipe(gulp.dest('dist'));

    gulp.src('app/img/**/*')
	.pipe(gulp.dest('dist/img'))

    gulp.src('app/js/main.min.js')
	.pipe(uglify())
    .pipe(gulp.dest('dist/js'));

});

gulp.task('default', ['watch']);

gulp.task('clear', function () {
    return cache.clearAll();
})